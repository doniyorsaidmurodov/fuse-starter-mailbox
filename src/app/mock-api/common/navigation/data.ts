/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id   : 'mailbox',
        title: 'Mailbox',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/mailbox'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'mailbox',
        title: 'Mailbox',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/mailbox'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'mailbox',
        title: 'Mailbox',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/mailbox'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'mailbox',
        title: 'Mailbox',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/mailbox'
    }
];
