import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ActionAdvancedSearchComponent} from "./advanced-search/advanced-search.component";

@Component({
  selector: 'app-action-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class ActionSearchComponent implements OnInit, OnDestroy {

  constructor(
    private _matDialog: MatDialog
    ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  advancedSearchDialog() {
    this._matDialog.open(ActionAdvancedSearchComponent, {
      panelClass: 'action-advanced-search-dialog',
      width: '1000px',
      maxWidth: '90vw'
    })
  }
}
