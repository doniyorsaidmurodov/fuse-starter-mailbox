import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-advanced-action-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss'],
})
export class ActionAdvancedSearchComponent implements OnInit, OnDestroy {

  constructor(
    public matDialogRef: MatDialogRef<ActionAdvancedSearchComponent>,
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}
