import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Subject, takeUntil} from 'rxjs';
import {FuseNavigationItem, FuseNavigationService, FuseVerticalNavigationComponent} from '@fuse/components/navigation';
import {MailboxService} from '../mailbox.service';
import {labelColorDefs} from '../mailbox.constants';
import {MailFilter, MailFolder, MailLabel} from '../mailbox.types';

@Component({
    selector: 'mailbox-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],

})
export class MailboxSidebarComponent implements OnInit, OnDestroy {
    @Input() name: string;
    filters: MailFilter[] = [];
    folders: MailFolder[] = [];
    labels: MailLabel[] = [];
    menuData: FuseNavigationItem[] = [];
    private _filtersMenuData: FuseNavigationItem[] = [];
    private _foldersMenuData: FuseNavigationItem[] = [];
    private _labelsMenuData: FuseNavigationItem[] = [];
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _mailboxService: MailboxService,
        private _matDialog: MatDialog,
        private _fuseNavigationService: FuseNavigationService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Filters
        this._mailboxService.filters$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filters: MailFilter[]) => {
                this.filters = filters;

                // Generate menu links
                this._generateFiltersMenuLinks();
            });

        // Folders
        this._mailboxService.folders$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((folders: MailFolder[]) => {
                this.folders = folders;

                // Generate menu links
                this._generateFoldersMenuLinks();

                // Update navigation badge
                this._updateNavigationBadge(folders);
            });

        // Labels
        this._mailboxService.labels$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((labels: MailLabel[]) => {
                this.labels = labels;

                // Generate menu links
                this._generateLabelsMenuLinks();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Generate menus for folders
     *
     * @private
     */
    private _generateFoldersMenuLinks(): void {
        // Reset the folders menu data
        this._foldersMenuData = [];

        // Iterate through the folders
        this.folders.forEach((folder) => {

            // Generate menu item for the folder
            const menuItem: FuseNavigationItem = {
                id: folder.id,
                title: folder.title,
                type: 'basic',
                icon: folder.icon,
                link: './' + folder.slug
            };

            // If the count is available and is bigger than zero...
            if (folder.count && folder.count > 0) {
                // Add the count as a badge
                menuItem['badge'] = {
                    title: folder.count + ''
                };
            }

            // Push the menu item to the folders menu data
            this._foldersMenuData.push(menuItem);
        });

        // Update the menu data
        this._updateMenuData();
    }

    /**
     * Generate menus for filters
     *
     * @private
     */
    private _generateFiltersMenuLinks(): void {
        // Reset the filters menu
        this._filtersMenuData = [];

        // Iterate through the filters
        this.filters.forEach((filter) => {

            // Generate menu item for the filter
            this._filtersMenuData.push({
                id: filter.id,
                title: filter.title,
                type: 'basic',
                icon: filter.icon,
                link: './filter/' + filter.slug
            });
        });

        // Update the menu data
        this._updateMenuData();
    }

    /**
     * Generate menus for labels
     *
     * @private
     */
    private _generateLabelsMenuLinks(): void {
        // Reset the labels menu
        this._labelsMenuData = [];

        // Iterate through the labels
        this.labels.forEach((label) => {

            // Generate menu item for the label
            this._labelsMenuData.push({
                id: label.id,
                title: label.title,
                type: 'basic',
                icon: 'heroicons_outline:tag',
                classes: {
                    icon: labelColorDefs[label.color].text
                },
                link: './label/' + label.slug
            });
        });

        // Update the menu data
        this._updateMenuData();
    }

    /**
     * Update the menu data
     *
     * @private
     */
    private _updateMenuData(): void {
        this.menuData = [
            {
                title: 'Mailboxes',
                type: 'collapsable',
                icon: 'heroicons_outline:mail',
                children: [...this._foldersMenuData]
            },
            {
                title: 'Filters',
                type: 'collapsable',
                icon: 'heroicons_outline:filter',
                children: [...this._filtersMenuData]
            },
            {
                title: 'Labels',
                type: 'collapsable',
                icon: 'heroicons_outline:tag',
                children: [...this._labelsMenuData]
            },
            {
                type: 'group',
                children: [
                    {
                        title: 'General',
                        type: 'collapsable',
                        icon: 'heroicons_outline:cog',
                        children: [
                            {
                                title: 'Tasks',
                                type: 'basic'
                            },
                            {
                                title: 'Users',
                                type: 'basic'
                            },
                            {
                                title: 'Teams',
                                type: 'basic'
                            }
                        ]
                    },
                    {
                        title: 'Account',
                        type: 'collapsable',
                        icon: 'heroicons_outline:user-circle',
                        children: [
                            {
                                title: 'Personal',
                                type: 'basic'
                            },
                            {
                                title: 'Payment',
                                type: 'basic'
                            },
                            {
                                title: 'Security',
                                type: 'basic'
                            }
                        ]
                    }
                ]
            },
        ];
    }

    /**
     * Update the navigation badge using the
     * unread count of the inbox folder
     *
     * @param folders
     * @private
     */
    private _updateNavigationBadge(folders: MailFolder[]): void
    {
        // Get the inbox folder
        const inboxFolder = this.folders.find(folder => folder.slug === 'inbox');

        // Get the component -> navigation data -> item
        const mainNavigationComponent = this._fuseNavigationService.getComponent<FuseVerticalNavigationComponent>('mainNavigation');

        // If the main navigation component exists...
        if ( mainNavigationComponent )
        {
            const mainNavigation = mainNavigationComponent.navigation;
            const menuItem = this._fuseNavigationService.getItem('mailbox', mainNavigation);

            // Update the badge title of the item
            menuItem.badge.title = inboxFolder.count + '';

            // Refresh the navigation
            mainNavigationComponent.refresh();
        }
    }
}
